# Backend Code Challenge 8

You've got 3 files: 

1. **client_matches**: A CSV file with events it's also database, but just was exported as CSV format.
2. **Settlements**: A snapshot of settlements for matches in the `client_matches file`, (basically a list results for all markets)
3. **Markets description** Small knowledgebase info you will need to associate outcomes correctly for home/away teams

The challenge is to manipulate the data and excerpt intelligence for all opponents you can find in the `client_matches` file, particularily the following statistics:

1. Total wins
2. Total losses
3. Total draws
4. Percentage (%) the team scored the first goal
5. Percentage (%) of matches for which the team was leading both halfs
6. Percentage (%) of matches for which the team was loosing both halfs

Challenge deliverables:

- Explain in structured fashion which steps you plan to do to generate the requested statistics
- Comment on which libraries/tools you will use for this purpose and why you make this choice
- Provide estimates on how much time it will take you to execute each step
